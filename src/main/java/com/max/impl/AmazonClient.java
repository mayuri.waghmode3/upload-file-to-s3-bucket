package com.max.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.max.service.AmazonClientService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@Service
@Transactional
public class AmazonClient implements AmazonClientService {
    private AmazonS3 s3Client;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;
    @Value("${amazonProperties.bucketName}")
    private String bucketName;
    @Value("${amazonProperties.accessKey}")
    private String accessKey;
    @Value("${amazonProperties.secretKey}")
    private String secretKey;

    @PostConstruct
    private void initializeAmazon() {
        BasicAWSCredentials creds = new BasicAWSCredentials(this.accessKey, this.secretKey);
        s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(Regions.US_EAST_1).build();
    }

    public String deletFileFromS3Bucket(String fileName){
        try {
            s3Client.deleteObject(new DeleteObjectRequest(bucketName, fileName));
        }catch (AmazonServiceException ex){
            System.out.println("error [" + ex.getMessage() + "] occurred while removing [" + fileName + "] ");
        }
        return "Successfully deleted";
    }

    public String uploadFile(MultipartFile multipartFile){
        String fileUrl = "";
        try{
            File file = convertMultipartToFile(multipartFile);
            String fileName = generateFileName(multipartFile);
            fileUrl = endpointUrl+"/"+bucketName+"/"+fileName;
            uploadFiletoS3Bucket(fileName,file);
            file.delete();
        }catch (Exception e){
            e.printStackTrace();
        }
        return fileUrl;
    }

    private File convertMultipartToFile(MultipartFile file) throws IOException {
        File convFile =  new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private String generateFileName(MultipartFile file){
        return new Date().getTime()+"-"+file.getOriginalFilename().replace(" ","_");
    }

    private void uploadFiletoS3Bucket(String fileName, File file){
        s3Client.putObject(
                new PutObjectRequest(bucketName,fileName,file)
                        .withCannedAcl(CannedAccessControlList.PublicRead)
        );
    }
}
