package com.max.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface AmazonClientService {
    String deletFileFromS3Bucket(String fileUrl);
    String uploadFile(MultipartFile multipartFile);
}
