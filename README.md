# Uploading/deleting files from AWS S3 Bucket using Spring Boot

## S3 Bucket configuration 

1)	Create bucket on S3 ->Enter your unique bucket name and choose region that is closest to you. 
2)	create new IAM user and give him permission(AmazonS3FullAccess) to use only S3 Bucket. Enter user’s name and check Access type ‘Programatic access’
3)	copy  Access key ID and Secret access key for your user.


## Spring Boot Part

###	add amazon dependency
    <dependency>
    <groupId>com.amazonaws</groupId>
    <artifactId>aws-java-sdk</artifactId>
    <version>1.11.133</version>
    </dependency>

###	Now let’s add s3 bucket properties to our application.yml file:
    amazonProperties:
    endpointUrl: https://s3.us-east-1.amazonaws.com
    accessKey: XXXXXXXXXXXXXXXXX
    secretKey: XXXXXXXXXXXXXXXXXXXXXXXXXX
    bucketName: your-bucket-name

##	Testing time
    Postman:
### endpoint: http://localhost:8080/storage/uploadFile.
    choose POST method, in the Body we should select ‘form-data’. As a key we should enter ‘file’ and choose value type ‘File’. Then choose any file from your PC as a value.
### endpoint: http://localhost:8080/storage/deleteFile.
    choose delete method, Body type is the same: ‘form-data’, key: ‘name’, and into value field enter the filename of file in S3 bucket.